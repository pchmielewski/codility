﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PermMissingElem
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] x = 
            {
                1, 3, 2, 5
            };
            var ordered = x.OrderBy(y => y);
            var min = ordered.Min();
            var max = ordered.Max();

            var table = new int[max];
            for (int i = 0; i < max; i++)
            {
                table[i] = min + i;
            }
            var b = table.Except(ordered).ToList();

        }
    }
}
